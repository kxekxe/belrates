from django.shortcuts import render, redirect
from django.shortcuts import render_to_response
from django.http import Http404, HttpResponse

def index(request):
	return render(request, 'index.html', locals())