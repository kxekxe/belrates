from django.db import models

# Create your models here.

class Bank(models.Model):
	active = models.BooleanField(default=True)
	name = models.CharField(max_length=255)
	link = models.URLField(max_length=255)
	url_path = models.CharField(max_length=255)
	phone1 = models.CharField(max_length=20)
	phone2 = models.CharField(max_length=20, blank=True, null=True)
	phone3 = models.CharField(max_length=20, blank=True, null=True)
	address = models.CharField(max_length=255, blank=True, null=True)
	opertime_from = models.DateTimeField(blank=True, null=True)
	opertime_to = models.DateTimeField(blank=True, null=True)
	lunchtime_from = models.DateTimeField(blank=True, null=True)
	lunchtime_to = models.DateTimeField(blank=True, null=True)

	def __unicode__(self):
		return self.name

	def get_absolute_url(self):
		return "/banks/%i/" % self.id

class Rates(models.Model):
	bank = models.ForeignKey(Bank)
	rate = models.DecimalField(max_digits=5, decimal_places=2)
	date = models.DateTimeField()
